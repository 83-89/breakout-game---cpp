#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <stdio.h>
#include "ShapeList.h"
#include "Brick.h"
#include "BrickWall.h"
#include "Ball.h"
#include "Impact.h"
#include "Paddle.h"
#include "point.h"
#include "rectangle.h"
#include "Shape.h"
#include "Player.h"
#include "Game.h"
#include "const.h"

#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

int main()
{
   al_init_primitives_addon();

   al_init_font_addon(); // initialize the font addon
   al_init_ttf_addon();// initialize the ttf (True Type Font) addon

   ALLEGRO_DISPLAY *display = NULL;
   bool doexit_game = false;
   bool doexit_menu = false;
   bool doexit = false;

   if(!al_init()) {
      fprintf(stderr, "failed to initialize allegro!\n");
      return -1;
   }

   if(!al_install_keyboard()) {
      fprintf(stderr, "failed to initialize the keyboard!\n");
      return -1;
   }

    if(!al_install_mouse()) {
      fprintf(stderr, "failed to initialize the mouse!\n");
      return -1;
   }

   display = al_create_display(SCREEN_W, SCREEN_H);
   if(!display) {
      fprintf(stderr, "failed to create display!\n");
      return -1;
   }

   ALLEGRO_FONT *font = al_load_ttf_font("pirulen.ttf",20,0 );
   ALLEGRO_FONT *fontGO = al_load_ttf_font("pirulen.ttf",60,0 );

   if (!font){
      fprintf(stderr, "Could not load 'pirulen.ttf'.\n");
      return -1;
   }

   al_clear_to_color(al_map_rgb(0, 0, 0));

   al_set_target_bitmap(al_get_backbuffer(display));

   al_clear_to_color(al_map_rgb(0,0,0));

   al_flip_display();

  ShapeList l;
  Ball *b;
  BrickWall *mb;
  Paddle *p;
  Player *pl;
  pl = new Player();
  Game *g;
  g = new Game();
  g->setPlayer(pl);
  g->setShapeList(&l);

    b = new Ball(SCREEN_W/2,SCREEN_H-20-3,6,-PAS);
    l.add(mb = new BrickWall(11,6));
    l.add(p = new Paddle(SCREEN_W/2-40,SCREEN_H-20,80,15,al_map_rgb(255,255,255),0.1));
    b->newBall(p);
    l.add(b);

     while(!doexit){

       while(!doexit_menu)
       {
            al_clear_to_color(al_map_rgb(0,0,0));
            al_draw_filled_rectangle(0,0,SCREEN_W,28,al_map_rgb(50,50,50));
            al_draw_text(font, al_map_rgb(255,255,255), SCREEN_W/2, 2,ALLEGRO_ALIGN_CENTER, "MENU");

            al_draw_text(font, al_map_rgb(255,255,255), SCREEN_W/2, SCREEN_H/2,ALLEGRO_ALIGN_CENTER, "PRESS ENTER TO CONTINUE");
            al_draw_text(font, al_map_rgb(255,255,255), SCREEN_W/2, SCREEN_H/2+30,ALLEGRO_ALIGN_CENTER, "PRESS END TO CLOSE");

            ALLEGRO_KEYBOARD_STATE keyState;
            al_get_keyboard_state(&keyState);

            if (al_key_down(&keyState, ALLEGRO_KEY_ENTER))
                doexit_menu=true;
            if (al_key_down(&keyState, ALLEGRO_KEY_END))
                doexit=doexit_menu=true;

            al_set_target_backbuffer(display);

            al_flip_display();
       }

       doexit_menu=false;

      while((!doexit_game))
       {
            al_clear_to_color(al_map_rgb(0,0,0));
            al_draw_filled_rectangle(0,0,SCREEN_W,28,al_map_rgb(50,50,50));

            char text_balls[50];
            char text_score[50];
            sprintf(text_balls, "Balls remaining : %d", pl->getLives()-1);
            if (pl->getLives()==0) sprintf(text_balls, "Balls remaining : --");
            sprintf(text_score, "Score : %d", pl->getScore());

            al_draw_text(font, al_map_rgb(255,200,200), 2, 2,ALLEGRO_ALIGN_LEFT, text_balls);

            al_draw_text(font, al_map_rgb(200,255,200), SCREEN_W/2+22, 2,ALLEGRO_ALIGN_LEFT, text_score);

            b->direction( b->testLimit(),pl);

            if (pl->getLives()<=0) {
                al_draw_text(fontGO, al_map_rgb(255,255,255), SCREEN_W/2, SCREEN_H/2,ALLEGRO_ALIGN_CENTER, "GAME OVER");
                al_draw_text(font, al_map_rgb(255,255,255), SCREEN_W/2, SCREEN_H-50,ALLEGRO_ALIGN_CENTER, "PRESS ESCAPE TO GO BACK TO THE MENU");
            }

            b->direction(b->collision(p));
            //p->move(b->getPasX());    //auto_mode

            mb->collision(b,pl);

            mb->isAlive();

            b->move();

            if ((b->getPasX()==0)&&(b->getPasY()==0)) b->newBall(p);

            ALLEGRO_KEYBOARD_STATE keyState;
            al_get_keyboard_state(&keyState);
            if ((al_key_down(&keyState, ALLEGRO_KEY_RIGHT))&&(p->getp2().getx()<640))
                {p->move(5);
                if ((b->getPasX()==0)&&(b->getPasY()==0)) b->move(5);}
            if ((al_key_down(&keyState, ALLEGRO_KEY_LEFT))&&(p->getp1().getx()>0))
                {p->move(-5);
                if ((b->getPasX()==0)&&(b->getPasY()==0)) b->move(-5);}
            if ((al_key_down(&keyState, ALLEGRO_KEY_UP))&&(b->getPasX()==0)&&(b->getPasY()==0)&&(pl->getLives()>0)){
                b->setPasX(PAS);b->setPasY(PAS);}
            if (al_key_down(&keyState, ALLEGRO_KEY_ESCAPE))
                doexit_game=true;
            if (al_key_down(&keyState, ALLEGRO_KEY_END))
                doexit=doexit_game=true;

            //mb->isAlive();

            if (mb->getCounter()==0){
                int bonus;
                if (pl->getLives()==3) bonus=30;
                else if (pl->getLives()==2) bonus=20;
                else bonus=10;
                pl->setScore(pl->getScore()+bonus);
                al_draw_text(fontGO, al_map_rgb(255,255,255), SCREEN_W/2, SCREEN_H/2,ALLEGRO_ALIGN_CENTER, "WINNER");
                al_draw_text(font, al_map_rgb(255,255,255), SCREEN_W/2, SCREEN_H-20,ALLEGRO_ALIGN_CENTER, "PRESS ESCAPE TO GO BACK TO THE MENU");
                b->setPasX(0);
                b->setPasY(0);
                mb->setCounter(-1);
            }

            g->getShapeList().display();

            al_set_target_backbuffer(display);

            al_flip_display();

            if (mb->getCounter()<0)
                for(int i=0;i<1000000000;i++)
                    if (al_key_down(&keyState, ALLEGRO_KEY_ESCAPE))
                        break;

       }

        doexit_game=false;

    }


   return 0;
}

