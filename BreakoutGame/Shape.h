    #pragma once
    class Shape{
    public:
	    Shape();
	    virtual ~Shape();
	    virtual void display() = 0;
    };
