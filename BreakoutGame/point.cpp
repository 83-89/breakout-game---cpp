#include "point.h"
#include <math.h>

point::point() {
	x = 0; y = 0;
}

point::point(float px, float py){
	x = px;  y = py;
}

point::~point() {}

void point::move(float dx, float dy){
	x = x + dx; y = y + dy;
}

void point::initialize(float px, float py){
	x = px;  y = py;
}

float point::distance(point p) {
    return sqrt(pow(p.getx() - x, 2) + pow(p.gety() - y, 2));
}

float point::calculateAngle(point p) {
	return atan2((y - p.gety()), (x - p.getx()));
}
