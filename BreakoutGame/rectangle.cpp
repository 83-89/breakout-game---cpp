#include "rectangle.h"

rectangle::rectangle(){}

rectangle::rectangle(float px, float py, float pl, float ph) : p1(px, py), p2(px + pl, py + ph){
}

rectangle::~rectangle(){}

void rectangle::init(float px1, float py1, float px2, float py2){
	p1.initialize(px1, py1);
	p2.initialize(px2, py2);
}

bool rectangle::selection(float sx, float sy)
{
	if ((sx >= p1.getx()) && (sx <= p2.getx()) && (sy >= p1.gety()) && (sy <= p2.gety())) return true;
	return false;
}
