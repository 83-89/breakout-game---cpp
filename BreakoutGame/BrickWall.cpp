#include "BrickWall.h"
#include "const.h"

BrickWall::BrickWall()
{
	float x, y;
	int i, j;
	srand(time(NULL));
	for (x = 30, i = 1;i <= 10;i++, x = x + 60)
		for (y = PANEL_H+5, j = 1;j <= 5;j++, y = y + 40){
			l.push_back(new Brick1(x, y, 45, 30, al_map_rgb(60 + ceil(rand() * 195), 60 + ceil(rand() * 195), 60 + ceil(rand() * 195)), 1));
			counter++;
			}
}

BrickWall::BrickWall(int colonnes, int lignes)
{
	float x, y;
	int i, j;
	srand(time(NULL));
	int h_gap, v_gap;
	h_gap = (SCREEN_W - colonnes * 50) / colonnes;
	v_gap = 5;
	for (x = h_gap / 2, i = 1;i <= colonnes;i++, x = x + h_gap + 50)
		for (y = PANEL_H+5, j = 1;j <= lignes;j++, y = y + v_gap + 15)
			//l.push_back(new brique(x,y,50,15,makecol(60+ceil(rand()*195),60+ceil(rand()*195),60+ceil(rand()*195)),10) );
			 if ((i+j)%2==0) l.push_back(new Brick1(x, y, 50, 15, al_map_rgb(255, 0, 255), 1));
			 else l.push_back(new Brick2(x, y, 50, 15, al_map_rgb(255, 0, 255), 1));
    counter=colonnes*lignes;
}

BrickWall::~BrickWall()
{
	for (it = l.begin(); it != l.end(); it++)
		delete (*it);
}

void BrickWall::display() {
	for (it = l.begin(); it != l.end(); it++)
		(*it)->display();
}

void BrickWall::collision(Ball * b,Player* pl) {
	for (it = l.begin(); it != l.end(); it++) {
		b->direction(b->collision(*it,pl));
	}
}

void BrickWall::isAlive() {
	for (it = l.begin(); it != l.end(); it++) {
		if ((*it)->getNbcollision() <= 0) {
			l.remove(*it);counter--;
            break;
		}
	}
}
