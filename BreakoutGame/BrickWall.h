#include "Shape.h"
#include "Brick.h"
#include "Brick1.h"
#include "Brick2.h"
#include "Ball.h"
#include <list>
#include <iterator>
#pragma once

using namespace std;

class BrickWall :public Shape{
protected:
	list <Brick*> l;
	list <Brick*>::iterator it;
	int counter=0;
public:
	BrickWall();
	BrickWall(int, int);
	~BrickWall();
	void display();
	void collision(Ball* b,Player* pl);
	void isAlive();
	int getCounter() {return counter;}
	void setCounter(int newCounterValue){counter=newCounterValue;}
};
