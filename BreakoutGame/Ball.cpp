#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <list>
#include <math.h>
#include <stdio.h>
#include "Ball.h"
#include "const.h"

Ball::Ball() :p(200, 200), rayon(10){
	pasx = pasy = 0;
}

Ball::Ball(float px, float py, float pr, float ppas) : p(px, py), rayon(pr){
	pasx = pasy = ppas;
}

void Ball::display(){
	al_draw_filled_circle(p.getx()+0.5f,p.gety()+0.5f,rayon,al_map_rgb(255,0,0));
}

int Ball::testLimit(){
	if (p.getx() - rayon + pasx <= 0) return 1;
	if (p.getx() + rayon + pasx >= SCREEN_W)  return 3;
	if (p.gety() - rayon + pasy <= PANEL_H) return 2;
	if (p.gety() + rayon + pasy >= SCREEN_H) return 4;
	return 0;
}

void Ball::direction(int pface,Player *pl) {
	if ((pface == 1) || (pface == 3)) pasx = -pasx;
	else if (pface == 2) pasy = -pasy;
	else if (pface == 4) {
        p.setx(SCREEN_W/2);
        p.sety(460-rayon);
        pasx=pasy=0;
        pl->setLives(pl->getLives()-1);
    }
}

void Ball::direction(Impact impact) {
	float tmpPasx = pasx, tmpPasy = pasy;
	float ballAngle = atan2(pasy, pasx);
	if ((impact.caseOfImpact == 1) || (impact.caseOfImpact == 3))
		pasx = -pasx;
	else if ((impact.caseOfImpact == 2) || (impact.caseOfImpact == 4))
		pasy = -pasy;

	else if (impact.caseOfImpact >= 5) {
		pasx = sqrt(pow(tmpPasx, 2) + pow(tmpPasy, 2)) * cos(2 * impact.angle - ballAngle - M_PI);
		pasy = sqrt(pow(tmpPasx, 2) + pow(tmpPasy, 2)) * sin(2 * impact.angle - ballAngle - M_PI);
	}
}

void Ball::move() {
	p.move(pasx, pasy);
}

void ResultOfCollision(Brick* br,Player* pl){
        br->setNbcollision(br->getNbcollision() - 1);
		br->action();
        pl->setScore(pl->getScore()+5);
}

Impact Ball::collision(Brick * br, Player* pl) {
	int caseOfImpact = 0;
	float angle=0;
	Impact impact;
	point p3(br->getp1().getx(), br->getp2().gety());    //coin inf�rieur gauche
	point p4(br->getp2().getx(), br->getp1().gety());    //coin sup�rieur droit

	if (br->selection(p.getx() + rayon+pasx, p.gety()+pasy)) {
        ResultOfCollision(br,pl);
		angle = M_PI;
		caseOfImpact = 1;
	}    //collision avec la face gauche
	else if (br->selection(p.getx()+pasx, p.gety() - rayon+pasy)) {
        ResultOfCollision(br,pl);
		angle = 3 * M_PI / 2;
		caseOfImpact = 2;
	}    //collision avec la face inf�rieure //attention � l'orientation de l'axe Y
	else if (br->selection(p.getx() - rayon+pasx, p.gety()+pasy)) {
        ResultOfCollision(br,pl);
		angle = 0;
		caseOfImpact = 3;
	}    //collision avec la face droite
	else if (br->selection(p.getx()+pasx, p.gety() + rayon+pasy)) {
        ResultOfCollision(br,pl);
		angle = M_PI / 2;
		caseOfImpact = 4;
	}    //collision avec la face sup�rieure //attention � l'orientation de l'axe Y
	else if (p.distance(br->getp1()) <= rayon) {
        ResultOfCollision(br,pl);
		angle = getP().calculateAngle(br->getp1());
		caseOfImpact = 5;
	}    //collision avec le coin sup�rieur gauche
	else if (p.distance(br->getp2()) <= rayon) {
        ResultOfCollision(br,pl);
		angle = getP().calculateAngle(br->getp2());
		caseOfImpact = 6;
	}    //collision avec le coin inf�rieur droit
	else if (p.distance(p3) <= rayon) {
        ResultOfCollision(br,pl);
		angle = getP().calculateAngle(p3);
		caseOfImpact = 7;
	}    //collision avec le coin inf�rieur gauche
	else if (p.distance(p4) <= rayon) {
        ResultOfCollision(br,pl);
		angle = getP().calculateAngle(p4);
		caseOfImpact = 8;
	}    //collision avec le coin sup�rieur droit

	impact.caseOfImpact = caseOfImpact;
	impact.angle = angle;
	return impact;
}

Impact Ball::collision(Paddle * br) {
	int caseOfImpact = 0;
	float angle;
	Impact impact;
	point p3(br->getp1().getx(), br->getp2().gety());    //coin inf�rieur gauche
	point p4(br->getp2().getx(), br->getp1().gety());    //coin sup�rieur droit

	if (br->selection(p.getx() + rayon+pasx, p.gety()+pasy)) {
		angle = M_PI;
		caseOfImpact = 1;
	}    //collision avec la face gauche
	else if (br->selection(p.getx()+pasx, p.gety() - rayon+pasy)) {
		angle = 3 * M_PI / 2;
		caseOfImpact = 2;
	}    //collision avec la face inf�rieure //attention � l'orientation de l'axe Y
	else if (br->selection(p.getx() - rayon+pasx, p.gety()+pasy)) {
		angle = 0;
		caseOfImpact = 3;
	}    //collision avec la face droite
	else if (br->selection(p.getx()+pasx, p.gety() + rayon+pasy)) {
		angle = M_PI / 2;
		caseOfImpact = 4;
	}    //collision avec la face sup�rieure //attention � l'orientation de l'axe Y
	else if (p.distance(br->getp1()) <= rayon) {
		angle = getP().calculateAngle(br->getp1());
		caseOfImpact = 5;
	}    //collision avec le coin sup�rieur gauche
	else if (p.distance(br->getp2()) <= rayon) {
		angle = getP().calculateAngle(br->getp2());
		caseOfImpact = 6;
	}    //collision avec le coin inf�rieur droit
	else if (p.distance(p3) <= rayon) {
		angle = getP().calculateAngle(p3);
		caseOfImpact = 7;
	}    //collision avec le coin inf�rieur gauche
	else if (p.distance(p4) <= rayon) {
		angle = getP().calculateAngle(p4);
		caseOfImpact = 8;
	}    //collision avec le coin sup�rieur droit

	impact.caseOfImpact = caseOfImpact;
	impact.angle = angle;
	return impact;
}

void Ball::move(float ppas) {
    p.move(ppas,0);
}

void Ball::newBall(Paddle *p){
    setP(point(p->getp1().getx()+(p->getp2().getx()-p->getp1().getx())/2,SCREEN_H-20-rayon));
    pasx=pasy=0;
}
