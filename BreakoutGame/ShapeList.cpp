#include "ShapeList.h"

ShapeList::ShapeList(){}

ShapeList::~ShapeList(){}

void ShapeList::display(){
	for (it = l.begin(); it != l.end(); it++)
		(*it)->display();
}

void ShapeList::add(Shape * p){
	l.push_back(p);
}

void ShapeList::remove(Shape * p){
	delete p;
	l.remove(p);
}
