#include "rectangle.h"
#include "allegro5/allegro.h"
#pragma once

class Brick :public rectangle {//rendre la classe Brick abstraite par rapport � certaines m�thodes (action ou int�raction � la collision diff�rente pour chaque type de Brick)
protected:
	ALLEGRO_COLOR color;
	int nbCollision;
public:
	Brick();
	Brick(float, float, float, float, ALLEGRO_COLOR, int);
	virtual ~Brick() {}
	virtual void display()=0;
	int getNbcollision() { return nbCollision; }
	void setNbcollision(int nbcoll) { nbCollision = nbcoll; }
	virtual int action()=0;
};
