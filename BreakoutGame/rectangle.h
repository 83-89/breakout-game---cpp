#include "Shape.h"
#include "point.h"
#pragma once

class rectangle :public Shape {
protected:
	point p1, p2;
public:
	rectangle();
	rectangle(float, float, float, float);
	~rectangle();
	void init(float, float, float, float);
	point getp1() { return p1; }
	point getp2() { return p2; }
	bool selection(float, float);
};
