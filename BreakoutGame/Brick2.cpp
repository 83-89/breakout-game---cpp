#include "Brick2.h"
#include "rectangle.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

void Brick2::display() {
	if (nbCollision!=1) al_draw_filled_rectangle(p1.getx(),p1.gety(),p2.getx()+1.0f,p2.gety()+1.0f,color);
    else al_draw_rectangle(p1.getx(),p1.gety(),p2.getx()+1.0f,p2.gety()+1.0f,color,2);
}

int Brick2::action(){
    p1.move(5,2);
    p2.move(-5,-2);
    return 0;
}
