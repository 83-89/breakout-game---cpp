#include "Brick.h"
#include "rectangle.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>

Brick::Brick() :rectangle()
{
	color = al_map_rgb(0,255,0);    
	nbCollision = 1;
}

Brick::Brick(float px, float py, float pl, float ph, ALLEGRO_COLOR pcolor, int pnbcol) :rectangle(px, py, pl, ph)
{
	color = pcolor;
	nbCollision = pnbcol;
}

