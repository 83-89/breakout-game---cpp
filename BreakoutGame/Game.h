#include "ShapeList.h"
#include "Player.h"
#pragma once

class Game{
    private :
        Player *pl;
        ShapeList *l;
    public :
        Game();
        ~Game();
        void setPlayer(Player*);
        void setShapeList(ShapeList*);
        Player getPlayer() {return *pl;}
        ShapeList getShapeList() {return *l;}
};
