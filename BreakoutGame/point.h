#include "Shape.h"
#include <math.h>
#pragma once

class point{
protected:
	float x, y;
public:
	point();
	point(float, float);
	~point();
	void move(float, float);
	void initialize(float, float);
	void display();
	float getx() { return x; }
	float gety() { return y; }
    void setx(float px){x=px;}
    void sety(float py){y=py;}
	float distance(point p);
	float calculateAngle(point);
};
