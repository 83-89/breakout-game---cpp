#include "Brick.h"
#include "allegro5/allegro.h"
#pragma once

class Brick2 :public Brick {//rendre la classe Brick abstraite par rapport � certaines m�thodes (action ou int�raction � la collision diff�rente pour chaque type de Brick)
public :
    Brick2():Brick(){}
    Brick2(float px, float py, float pl, float ph,ALLEGRO_COLOR pcoul,int pnbcol):Brick(px,py,pl,ph,pcoul,pnbcol) {
       color=al_map_rgb(0,255,255);
       nbCollision = 2;
    }
    ~Brick2() {}
    void display();
    int action();
};

