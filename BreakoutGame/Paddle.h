#include "rectangle.h"
#include <allegro5/allegro_primitives.h>
#pragma once

class Paddle :public rectangle
{
protected:
	ALLEGRO_COLOR color;
	float pasx;
public:
	Paddle();
	Paddle(float, float, float, float, ALLEGRO_COLOR, float);
	~Paddle() {}
	void display();
	void move(float);
};
