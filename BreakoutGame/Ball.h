#include "Shape.h"
#include "point.h"
#include "Brick.h"
#include "Impact.h"
#include "Paddle.h"
#include "Player.h"
#pragma once

class Ball: public Shape {
protected:
	point p;
	float rayon, pasx, pasy;
public:
	Ball();
	Ball(float, float, float, float);
	~Ball() {}
	void display();
	void direction(int,Player*);
	void direction(Impact);
	void move();
	void move(float);
	bool selection(float, float);
	int testLimit();
	Impact collision(Brick*, Player*);
	Impact collision(Paddle*);
	point getP() {return p;}
    float getRayon(){return rayon;}
    float getPasX(){return pasx;}
    float getPasY(){return pasy;}
    void setPasX(float ppasx){pasx=ppasx;}
    void setPasY(float ppasy){pasy=ppasy;}
    void setP(point pt) {p=pt;}
    void newBall(Paddle*);
};
