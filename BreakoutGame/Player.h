
#pragma once

class Player {
    protected :
        int score;
        int lives;
    public :
        Player();
        Player(int,int);
        ~Player();
        int getLives() {return lives;}
        void setLives(int);
        int getScore() {return score;}
        void setScore(int);
};
