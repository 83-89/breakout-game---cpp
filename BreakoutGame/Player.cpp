#include "Player.h"

Player::Player(){
    score=0;
    lives=3;
}

Player::Player(int pScore,int pLives){
    score=pScore;
    lives=pLives;
}

Player::~Player(){}

void Player::setLives(int pLives){
    lives=pLives;
}

void Player::setScore(int pScore){
    score=pScore;
}
