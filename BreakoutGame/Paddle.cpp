#include "Paddle.h"

Paddle::Paddle() :rectangle(){
	color = al_map_rgb(255,255,255);

}

Paddle::Paddle(float px, float py, float pl, float ph, ALLEGRO_COLOR pcolor, float ppasx) :rectangle(px, py, pl, ph) {
	color = pcolor;
	pasx = ppasx;
}

void Paddle::display(){
	al_draw_filled_rectangle(p1.getx(),p1.gety(),p2.getx()+1.0f,p2.gety()+1.0f,color);
}

void Paddle::move(float pas){
	p1.move(pas, 0);
	p2.move(pas, 0);
}
