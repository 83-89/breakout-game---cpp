#include "Shape.h"
#include <list>
#include <iterator>
#pragma once

using namespace std;

class ShapeList{
protected:
	list <Shape*> l;
	list <Shape*>::iterator it;
public:
	ShapeList();
	~ShapeList();
	void add(Shape*);
	void remove(Shape*);
	void display();
	int fsize() { return l.size(); }
};
